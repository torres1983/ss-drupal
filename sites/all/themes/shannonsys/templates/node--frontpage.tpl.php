<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$imagepath=path_to_theme();
drupal_add_css($imagepath.'/css/index.css');
?>
<div class="g-home-banner">
        <ul class="home-banner-list">
            <li class="home-banner-item">
                <div class="w-home-banner-item-media"><img data-src="assets/images/8639.jpg" alt="" class="home-banner-item-media home-banner-item-pic"></div>
                <div class="w-home-banner-item-text">
                    <h2 class="home-banner-item-title_1">宝存产品</h2>
                    <h2 class="home-banner-item-title_2 font-HelveticaRoman">SFF-8639接口PCIe Flash闪存现已问世</h2>
                </div>
            </li>
            <li class="home-banner-item">
                <div class="w-home-banner-item-media"><img data-src="assets/images/6.4.jpg" alt="" class="home-banner-item-media home-banner-item-pic"></div>
                <div class="w-home-banner-item-text">
                    <h2 class="home-banner-item-title_1">宝存产品</h2>
                    <h2 class="home-banner-item-title_2 font-HelveticaRoman">全球最大容量6.4TB PCIe Flash正式发布</h2>
                </div>
            </li>
            <li class="home-banner-item" onClick="location.href='page-aboutus-4.html'" style="cursor:pointer;">
                <div class="w-home-banner-item-media"><img data-src="assets/images/home-banner-3.jpg" alt="" class="home-banner-item-media home-banner-item-pic"></div>
                <div class="w-home-banner-item-text">
                    <h2 class="home-banner-item-title_1">新闻</h2>
                    <h2 class="home-banner-item-title_2 font-HelveticaRoman">宝存科技赞助2014年美国圣克拉拉闪存存储峰会</h2>
                </div>
            </li>
        </ul>
        <ul class="home-banner-icon-list">
            <li class="home-banner-icon-item home-banner-icon-item_current"><a href="javascript:void(0);" class="home-banner-icon"></a></li>
            <li class="home-banner-icon-item"><a href="javascript:void(0);" class="home-banner-icon"></a></li>
            <li class="home-banner-icon-item"><a href="javascript:void(0);" class="home-banner-icon"></a></li>
        </ul>
    </div>
    <div class="g-home-products">
        <a class="home-products-navBtn home-products-navBtn_left" href="javascript:void(0)"></a>
        <a class="home-products-navBtn home-products-navBtn_right" href="javascript:void(0)"></a>
        <img class="home-product-360tip" src="css/images/home/arrow-360.png" height="33" width="102"/>
        <div class="home-section-title-1 home-section-title-1_products"><span class="home-section-title-1_en font-HelveticaNeueUL">PRODUCTS</span><em class="home-section-title-1_sep"></em><span class="home-section-title-1_cn">宝存产品</span></div>
        <div class="w-home-products-title">
            <div class="w-home-products-title-list">
                <ul class="home-products-title-list">
                    <li class="home-products-title"><a href="">Shannon Direct-IO™ PCIe Flash</a></li>
                    <li class="home-products-title"><a href="">Shannon Direct-IO™ PCIe Flash</a></li>
                    <li class="home-products-title"><a href="">Shannon Direct-IO™ PCIe Flash</a></li>
                </ul>
                
            </div>
            <!-- <div class="home-products-title-mask"></div> -->
        </div>
        <div class="w-home-products-360">
            <div class="home-products-360-item">
                <div class="home-products-360-item-holder g-w1000" id="image_holder_1">
                    <img class="home-products-360-item-info" id="product_image_1" src="assets/products/p1/p.png"  >
                </div>
            </div>
            <div class="home-products-360-item">
                <div class="home-products-360-item-holder g-w1000" id="image_holder_2">
                    <img class="home-products-360-item-info" id="product_image_2" src="assets/products/p2/p.png"  >
                </div>
            </div>
            <div class="home-products-360-item">
                <div class="home-products-360-item-holder g-w1000" id="image_holder_3">
                    <img class="home-products-360-item-info" id="product_image_3" src="assets/products/p3/p.png"  >
                </div>
            </div>
        </div>
    </div>

    <div class="g-home-technology">
        <div class="home-section-title-1 home-section-title-1_technology">
            <span class="home-section-title-1_en font-HelveticaNeueUL">TECHNOLOGY</span><em class="home-section-title-1_sep"></em><span class="home-section-title-1_cn">技术</span>
            <div class="home-technology-book"><a href="page-technology-1.html" class="home-technology-book-link">技术白皮书</a></div>
        </div>
        <ul class="home-technology-list">
            <li class="home-technology-item">
                <i class="home-technology-sep home-technology-sep_right"></i><a href="page-technology-1.html" class="home-technology-item-link">针对数据库优化</a>
            </li><li class="home-technology-item">
                <a href="page-technology-2.html" class="home-technology-item-link">数据安全性第一</a>
            </li>
        </ul>
    </div>

    <div class="g-home-solutions">
        <div class="home-section-title-1 home-section-title-1_solutions font-HelveticaNeueUL g-w1000"><span class="home-section-title-1_en">SOLUTIONS</span><em class="home-section-title-1_sep"></em><span class="home-section-title-1_cn">解决方案</span></div>
        <ul class="home-solutions-list g-w1000">
            <li class="home-solutions-item">
                <a href="page-solution-1.html" class="home-solutions-item-pic-link"><img src="css/images/home/icon-solution-0.png" height="64" width="62" alt="" class="home-solutions-item-pic"></a>
                <a href="page-solution-1.html" class="home-solution-item-title-link">数据库解决方案</a>
                <p class="home-solution-item-subtitle"><span class="home-solution-item-subtitle_cn">高可用数据库应用解决方案</span><br/><span class="home-solution-item-subtitle_en">ACTIVE HOT STANDBY etc.</span></p>
            </li><li class="home-solutions-item">
                <em class="home-solutions-item-sep home-solutions-item-sep_left"></em>
                <em class="home-solutions-item-sep home-solutions-item-sep_right"></em>
                <a href="javascript:;" class="home-solutions-item-pic-link"><img src="css/images/home/icon-solution-1.png" alt="" class="home-solutions-item-pic"></a>
                <a href="javascript:;" class="home-solution-item-title-link">虚拟化解决方案</a>
                <!-- <p class="home-solution-item-subtitle"><span class="home-solution-item-subtitle_cn">高可用数据库应用解决方案</span><br/><span class="home-solution-item-subtitle_en">ACTIVE HOT STANDBY etc.</span></p> -->
            </li><li class="home-solutions-item">
                <a href="javascript:;" class="home-solutions-item-pic-link"><img src="css/images/home/icon-solution-2.png" alt="" class="home-solutions-item-pic"></a>
                <a href="javascript:;" class="home-solution-item-title-link">大数据解决方案</a>
                <!-- <p class="home-solution-item-subtitle"><span class="home-solution-item-subtitle_cn">高可用数据库应用解决方案</span><br/><span class="home-solution-item-subtitle_en">ACTIVE HOT STANDBY etc.</span></p> -->
            </li>
        </ul>
    </div>
    
    <div class="g-home-ss">
        <div class="w-home-section-title-2 g-w1000">
            <h2 class="home-section-title-2_cn">技术支持</h2>
            <h2 class="home-section-title-2_en font-HelveticaNeueUL">SUPPORT AND SERVICE</h2>
        </div>
        <div class="w-home-ss-description g-w1000">
            <h2 class="home-ss-subtitle">欢迎进入宝存科技支持页面</h2>
            <p class="home-ss-subdescrip">宝存科技致力于向您提供更加专业的服务，您可通过以下方式获取更多内容。</p>
        </div>
        <ul class="w-home-ss-list g-w1000">
            <li class="home-ss-item"><a href="page-service-3.html" class="home-ss-link">查看详细</a>
            <!-- <li class="home-ss-item"><a href="page-service-1.html" class="home-ss-link">文件支持及下载</a></li>
            <li class="home-ss-item"><a href="page-service-2.html" class="home-ss-link">技术支持</a></li>
            <li class="home-ss-item"><a href="page-service-3.html" class="home-ss-link">资料申请表格</a></li> -->
        </ul>
    </div>

    <div class="g-home-aboutus">
        <div class="w-home-section-title-2 g-w1000">
            <h2 class="home-section-title-2_cn">关于我们</h2>
            <h2 class="home-section-title-2_en font-HelveticaNeueUL">ABOUT US</h2>
        </div>
        <ul class="home-aboutus-list g-w1000">
            <li class="home-aboutus-item"><a href="page-aboutus-1.html" class="home-aboutus-link">
                    <img src="css/images/home/pic-aboutus-1.jpg" alt="" class="home-aboutus-pic">
                    <h2 class="home-aboutus-subtitle">公司信息</h2>
                </a></li>
            <li class="home-aboutus-item"><a href="page-aboutus-2.html" class="home-aboutus-link">
                    <img src="css/images/home/pic-aboutus-2.jpg" alt="" class="home-aboutus-pic">
                    <h2 class="home-aboutus-subtitle">招聘信息</h2>
                </a></li>
            <li class="home-aboutus-item"><a href="page-aboutus-3.html" class="home-aboutus-link">
                    <img src="css/images/home/pic-aboutus-3.jpg" alt="" class="home-aboutus-pic">
                    <h2 class="home-aboutus-subtitle">联系方式</h2>
                </a></li>
            <li class="home-aboutus-item"><a href="page-aboutus-4.html" class="home-aboutus-link">
                    <img src="css/images/home/pic-aboutus-4.jpg" alt="" class="home-aboutus-pic">
                    <h2 class="home-aboutus-subtitle">宝存新闻</h2>
                </a></li>
        </ul>
    </div>





<div class="kv-blue">
    <div class="g-wrap1000">
        <h6>the global board</h6>
    </div>
</div>
<div class="vice">
    <div class="g-wrap1000">
      <div class="vice-text">
        <?php print $node->body['und'][0]['value']; ?>
      </div>
    </div>
</div>
<div class="w-corporate">
    <div class="g-wrap1000">
        <?php 
			for($i=0;$i<count($content['field_corporate']['#items']);$i++) { 
				$j = $node->field_corporate['und'][$i]['value'];
		?>
        	<div class="g-corporate">
                <div class="corp-left"><img src="<?php print file_create_url($content['field_corporate'][$i]['entity']['field_collection_item'][$j]['field_board_members_img']['#items'][0]['uri']); ?>" class="corp-img"></div>
                <div class="corp-right">
                    <p class="corp-name"><?php print $content['field_corporate'][$i]['entity']['field_collection_item'][$j]['field_board_members']['#items'][0]['value'];?></p>
                    <?php print $content['field_corporate'][$i]['entity']['field_collection_item'][$j]['field_board_members_introduction']['#items'][0]['value'];?>
                </div>
            </div>
        <?php
				if(($i%2) == 1) { print '<div class="clearfix"></div>'; }
			}
		?>
    </div>
</div>