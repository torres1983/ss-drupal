<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
$imagepath=drupal_get_path('theme', 'shannonsys');
?>
<div class="g-site-header">
    <div class="w-site-header">
      <div class="site-header">
        <a href="index.html" class="w-logo-link"><img src="<?=$imagepath?>/css/images/logo.png" height="39" width="249" alt="logo" class="logo-pic"></a>
        <div class="w-site-nav">
          <div class="g-site-lang"><div class="site-lang"><a href="javascript:;" class="site-lang-link">EN</a><em class="site-lang-sep"></em><a href="" class="site-lang-link site-lang-link_current">CN</a></div></div>
          <ul class="site-nav">
            <!--请保证单行输出这些item-->
            <li class="site-nav-item site-nav-item_current">
              <a href="index.html" class="site-nav-link">首页</a>
              <div class="site-nav-gap-bridge"></div>
            </li><li class="site-nav-item">
              <a href="page-production-1.html" class="site-nav-link">产品</a>
              <div class="site-nav-gap-bridge"></div>
              <ul class="site-subnav">
                <li class="site-subnav-item"><a href="page-production-1.html" class="site-subnav-link">闪存存储卡</a></li>
                <li class="site-subnav-item"><a href="javascript:;" class="site-subnav-link">闪存存储阵列</a></li>
                <!--<li class="site-subnav-item"><a href="page-production-3.html" class="site-subnav-link">存储系统</a></li>-->
              </ul>
            </li><li class="site-nav-item">
              <a href="page-technology-1.html" class="site-nav-link">技术</a>
              <div class="site-nav-gap-bridge"></div>
              <ul class="site-subnav">
                <li class="site-subnav-item"><a href="page-technology-1.html" class="site-subnav-link">针对数据库优化</a></li>
                <li class="site-subnav-item"><a href="page-technology-2.html" class="site-subnav-link">数据安全性第一</a></li>
              </ul>
            </li><li class="site-nav-item">
              <a href="page-solution-1.html" class="site-nav-link">解决方案</a>
              <div class="site-nav-gap-bridge"></div>
              <ul class="site-subnav">
                <li class="site-subnav-item"><a href="page-solution-1.html" class="site-subnav-link">数据库</a></li>
                <li class="site-subnav-item"><a href="page-solution-2.html" class="site-subnav-link">虚拟化</a></li>
                <li class="site-subnav-item"><a href="page-solution-3.html" class="site-subnav-link">大数据解决方案</a></li>
              </ul>
            </li><li class="site-nav-item">
              <a href="page-service-3.html" class="site-nav-link">技术支持</a>
              <div class="site-nav-gap-bridge"></div>
              <!--<ul class="site-subnav">
                <li class="site-subnav-item"><a href="page-service-1.html" class="site-subnav-link">文件支持及下载</a></li>
                <li class="site-subnav-item"><a href="page-service-2.html" class="site-subnav-link">技术支持</a></li>
                <li class="site-subnav-item"><a href="page-service-3.html" class="site-subnav-link">资料申请表格</a></li>
              </ul>-->
            </li><li class="site-nav-item">
              <a href="page-aboutus-1.html" class="site-nav-link">关于我们</a>
              <div class="site-nav-gap-bridge"></div>
              <ul class="site-subnav">
                <li class="site-subnav-item"><a href="page-aboutus-1.html" class="site-subnav-link">公司信息</a></li>
                <li class="site-subnav-item"><a href="page-aboutus-2.html" class="site-subnav-link">招聘信息</a></li>
                <li class="site-subnav-item"><a href="page-aboutus-3.html" class="site-subnav-link">联系方式</a></li>
                <li class="site-subnav-item"><a href="page-aboutus-4.html" class="site-subnav-link">宝存新闻</a></li>
              </ul>
            </li>
            <li class="site-nav-currentTag"></li>
          </ul>
        </div>
      </div>
    </div>
  </div><!-- g-site-header -->

<?php print render($page['content']); ?>

<div class="g-site-footer">
    <ul class="site-footer">
      <li class="site-footer-item"><a href="page-friends.html" class="site-footer-item-link">合作伙伴</a></li>
            <li class="site-footer-item"><a href="page-clients.html" class="site-footer-item-link">我们的客户</a></li>
      <li class="site-footer-item"><a href="page-aboutus-2.html" class="site-footer-item-link">加入我们</a></li>
      <li class="site-footer-item"><a href="page-industry-news.html" class="site-footer-item-link">行业动态</a></li>
      <li class="site-footer-item"><a href="page-sitemap.html" class="site-footer-item-link">网站地图</a></li>
      <li class="site-footer-item"><a href="page-aboutus-3.html" class="site-footer-item-link">关注宝存</a></li>
      <li class="site-footer-item"><a href="page-term.html" class="site-footer-item-link">隐私保护</a></li>
    </ul>
  </div>
  <?php 
    drupal_add_js($imagepath.'/js/jquery-1.11.0.min.js', array('scope' => 'footer',));
    drupal_add_js($imagepath.'/js/global.min.js', array('scope' => 'footer',));
    drupal_add_js($imagepath.'/js/query.fadeSlider.min.js', array('scope' => 'footer',));
    drupal_add_js($imagepath.'/js/jquery.360viewer.min.js', array('scope' => 'footer',));
    drupal_add_js($imagepath.'/js/index.js', array('scope' => 'footer',));
  ?>
