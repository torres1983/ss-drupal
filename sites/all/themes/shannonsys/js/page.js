$(function(){
	var $window = $(window),
		$body = $('body'),
		$page_nav_wrapper = $('.g-page-nav'),
		$page_nav = $('.w-page-nav'),
		$page_content_nav_wrapper = $('.w-page-content-nav'),
		$page_content_nav = $('.page-content-nav'),
		$page_content_wrapper = $('.g-page-content'),
		//page_content_nav_height = $page_content_nav.height(),
		page_nav_fixed_class = 'w-page-nav_fixed',
		page_content_nav_fixed_class = 'page-content-nav_fixed',
		page_content_nav_absolute_class = 'page-content-nav_absolute';
	
	$window.on('scroll',function(e){
		/*内页内容导航*/
		if($page_content_nav_wrapper.offset().top - $window.scrollTop() - 60 <0.5){
			var limite = $page_content_wrapper.height()+$page_content_wrapper.offset().top-$page_content_nav.height()-$window.scrollTop();
			//console.log(limite);
			if(limite<60){
				$page_content_nav.addClass(page_content_nav_absolute_class);
				$page_content_nav.removeClass(page_content_nav_fixed_class);
			}else {
				$page_content_nav.removeClass(page_content_nav_absolute_class);
				$page_content_nav.addClass(page_content_nav_fixed_class);
			}

		} else if($page_content_nav_wrapper.offset().top - $window.scrollTop() - 60 >0.5){
			$page_content_nav.removeClass(page_content_nav_fixed_class);
		}
	});

	$('.page-content-nav-item_current').on('click',function(e){
		$(this).toggleClass('page-content-nav-item_close');
		return false;
	});
	$('.page-content-subnav-list').on('click',function(e){
		e.stopPropagation();
		return false;
	});
	$('.page-content-subnav-link').on('click',function(e){
		var $this = $(this),
			$target = $($this.attr('href'));
		$body.animate({'scrollTop':$target.offset().top-60},{queue:false,duration:200});
		return false;
	});
});