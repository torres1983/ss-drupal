$(function() {

	//360 viewer
	var $product_items = $('.home-products-360-item'),
		product_items_length = $product_items.length,
		current_product_index = 0,
		product_info = [];
		
	
	// products title
	var $product_titles = $('.home-products-title');

	//init
	for(var i = 0; i < product_items_length; i++){
		var pi = {};
		pi.viewer = new jq360Viewer($('#product_image_'+(i+1)),{
			total_frames:12,
			target_id:'image_holder_'+(i+1)
		});
		pi.$item = $product_items.eq(i);
		pi.$title = $product_titles.eq(i);
		pi.started = false;
		product_info[i] = pi;
	}

	switchProduct(1);

	$('.home-products-navBtn_left').on('click',function(e){
		switchProduct(-1);
	});
	$('.home-products-navBtn_right').on('click',function(e){
		switchProduct(+1);
	});
		

	function switchProduct(direction){
		var index = current_product_index + direction,
			pi_animate_class = 'home-products-360-item_show',
			curr_pi = product_info[current_product_index],
			next_pi;

		if(index < 0){
			index = product_items_length-1;
		}
		if(index >= product_items_length){
			index = 0;
		}

		next_pi = product_info[index];

		curr_pi.$item.removeClass(pi_animate_class);
		if (!next_pi.started) {
			next_pi.viewer.start();
			
			next_pi.started = true;
		} else {
			next_pi.viewer.rotateToNumber(0); //重置回第一张
		}

		next_pi.$item.addClass(pi_animate_class);

		next_pi.$title.fadeIn(400);
		curr_pi.$title.fadeOut(400);

		current_product_index = index;

	}

	//banner
	$('.home-banner-list').fadeSlider({
		interval: 6000/*,
		animationStartCallback: function($item){
			var $title = $item.find('.w-home-banner-item-text');

				$title.removeClass('home-banner-item-title_show');
		},
		animationEndCallback: function($item){
			var $title = $item.find('.w-home-banner-item-text');

				$title.addClass('home-banner-item-title_show');
		}*/
	});

 });

