$(function(){
	$('.page-content-banner-item').bannerSlider({
		bannerPicClass: 'page-content-banner-pic',
		autoTime: 6000,
		$nextBtn: $('.page-content-banner-switchBtn_right'),
		$prevBtn: $('.page-content-banner-switchBtn_left')
	});
});