$(function(){
	var $window = $(window),
		site_header_fixed_class = 'w-site-header_fixed',
		$site_header = $('.w-site-header'),
		isFixed = false;

	$window.on('scroll',function(e){
		var top = $window.scrollTop();

		if(top>=110 && !isFixed){
			isFixed = true;
			$site_header.css({'opacity':0}).addClass(site_header_fixed_class).animate({'opacity':1},{queue:false,duration:400});
		}
		if(top<=60 && isFixed){
			isFixed = false;
			$site_header.removeClass(site_header_fixed_class);
		}
	});
});