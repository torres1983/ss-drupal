/*

*/
(function($){
	$.fn.fadeSlider = function(options){
		var opts = $.extend({
			bannerItemSelector: '.home-banner-item',
			bannerIconSelector: '.home-banner-icon-item',
			mediaSelector: '.home-banner-item-media',
			currentIconClass: 'home-banner-icon-item_current',
			currentiIndex: 0,
			speed: 600,
			interval: 6000,
			animationStartCallback: function($item){},
			animationEndCallback: function($item){}
		}, options);

		// 初始化
		var interval = opts.interval,
			curIndex = opts.currentiIndex,
			$contents = this.find(opts.bannerItemSelector),
			$icons = $(opts.bannerIconSelector),
			$first = $contents.eq(curIndex).css('opacity','1'),
			$loading,tId;

		loadImg(curIndex);/**/
		$icons.on("click",function(){
			
			var $this = $(this);
			var index = $this.index();
			switchCont(index);
		});

		function switchCont(index) {
			if(index == curIndex){
				return;
			} else {
				loadImg(index);
			}
		}
		function contFade($cont,index){
			var $curCont = $contents.eq(curIndex);
			$icons.eq(curIndex).removeClass(opts.currentIconClass);
			$icons.eq(index).addClass(opts.currentIconClass);
			$curCont.css('z-index','7');
			opts.animationStartCallback($curCont);
			$cont.css('z-index','8').animate({ 'opacity': 1 }, { queue: false, duration: opts.speed, complete:function(){
				curIndex != index && $curCont.css({'opacity':'0','z-index':'7'});
				curIndex = index;
				tId = autoPlay();
				opts.animationEndCallback($cont);
			}});
		}

		function autoPlay(){
			if(interval){
				clearTimeout(tId);
				return window.setTimeout(function(){
					var index = curIndex + 1;
					if(index >= $contents.length){
						index = 0;
					}
					switchCont(index);
				},interval);
			}
		}
		function loadImg(index){
			var $cont = $contents.eq(index),
				$media = $cont.find(opts.mediaSelector),
				realSrc = $media.attr("data-src");
			if($media.attr("src")){
				contFade($cont,index);
				return;
			} else {
				$media.attr("src",realSrc).load(function(){
					/*if(index === 0){
						tId = autoPlay();
					} else {*/
						contFade($cont,index);
					/*}*/

				});
			}
		}
	};
})(jQuery);