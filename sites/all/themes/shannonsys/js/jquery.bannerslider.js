;(function($){

	$.fn.bannerSlider = function(options){
		var opts = $.extend({}, $.fn.bannerSlider.defaults, options); 
			currentHandleClass = opts.currentHandleClass,
			bannerPicClass = opts.bannerPicClass,
			currentIndex = 0,
			autoTimeoutId = 0,
			autoTime = opts.autoTime,
			$bannerItems = this,
			$bannerHandleItems =  $(opts.handleItemClass ? '.'+opts.handleItemClass : ''),
			loadBanner = function(index){
				var $banner = $bannerItems.eq(index),
					$pic = $banner.find('.'+bannerPicClass);
				$pic.attr('src',$pic.attr('data-src'));
				return $bannerItems.eq(index);
			},
			autoSlideBanner = function(){
				var length = $bannerItems.length,
					nextIndex = currentIndex+1 >= length ? 0 : currentIndex+1;
				autoTimeoutId = setTimeout(function(){
					gotoBanner(nextIndex,+1);
				},autoTime);
			},
			switchBannerHandleClass = function(index){
				$bannerHandleItems.eq(currentIndex).removeClass(currentHandleClass);
				$bannerHandleItems.eq(index).addClass(currentHandleClass);
			},
			gotoBanner = function(index,direction){
				var $curBanner = $bannerItems.eq(currentIndex),
					$gotoBanner = $bannerItems.eq(index),
					distance = 100;//index>currentIndex?opts.slideDistance:-opts.slideDistance;

				clearTimeout(autoTimeoutId);
				$gotoBanner.css('left', distance*direction+'%').stop(true).animate({'left':'0'}, 400, 'swing');
				$curBanner.stop(true).animate({'left': -distance*direction+'%'}, 400, 'swing');
				
				switchBannerHandleClass(index);
				loadBanner(index);
				currentIndex = index;

				autoSlideBanner();
			};


			
			loadBanner(currentIndex).css('left','0');

			$bannerHandleItems.eq(currentIndex).addClass(currentHandleClass);

			$bannerHandleItems.each( function(i,o){
				var $handle = $(o);
				$handle.on('mouseenter',function(event){
					if(!$handle.hasClass(currentHandleClass)){
						clearTimeout(autoTimeoutId);
						gotoBanner(i,+1);
					}
				});
			});

			autoSlideBanner();

			opts.$prevBtn.on('click',function(e){
				var index = currentIndex - 1;
				
				index = index < 0 ? $bannerItems.length-1 : index;
				/*if(index<0){
					index = $bannerItems.length-1;
				}else{
					index = index;
				}*/

				gotoBanner(index,-1);
				return false;
			});

			opts.$nextBtn.on('click',function(e){
				var index = currentIndex + 1;
				
				index = index >= $bannerItems.length ? 0 : index;

				gotoBanner(index,+1);
				return false;
			});

			return this;
	};

	$.fn.bannerSlider.defaults = {
		autoTime: 6000,
		$nextBtn: $(),
		$prevBtn: $()
	};

})(jQuery);