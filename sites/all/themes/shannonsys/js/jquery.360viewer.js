jQuery(document).ready(function($) {
    jq360Viewer = function(element, options) {
        this.el = element;
        this.$el = $(element);
        this.options = $.extend({}, this.defaults, options);
        $.extend(this.options, this.$el.data());
        this.targetEl;
        this.started = false;
        this.defaultsCheck();
        this.imagesPath, this.imagesSrc = Array(), this.images = Array(), this.autoFind = true;
        this.oldImage = -1, this.currentImage = 0, this.numImages;
        this.focusable = false;
        this.degrees = 0, this.speedDeg = 0, this.speedInc = 0, this.speedDegDefault = 0;
        this.dragging = false;
        this.positionClickedX, this.degWhenClicked;
        this.currentX, this.oldDif;
        this.ease, this.inertia;
        this.autoplay = false, this.autoplaySpeed;
        this.reverse = false;
        this.width, this.height;
        this.iniWidth, this.iniHeight;
        this.posX = 0, this.posY = 0;
        this.$root = $(this.targetEl);
        this.$main = $("<div></div>");
        this.$view = $("<div></div>");
        this.$imagesHolder = $("<div></div>");
        this.goToDegree = false, this.focusedNum = -1;
    };
    jq360Viewer.prototype = {defaults: {ease: 8,inertia: 1.5,reverse: false,total_frames: null,target_id: null,just_load: false,images_src: []},_this: this,start: function() {
            if (this.started) {
                return;
            }
            this.width = this.getParam("viewWidth", this.targetEl.width());
            this.height = this.getParam("viewHeight", this.targetEl.height());
            this.ease = this.getParam("ease");
            this.paddingEase = this.getParam("padding_ease");
            this.inertia = this.getParam("inertia");
            this.reverse = this.getParam("reverse");
            this.autoplay = this.getParam("autoplay");
            this.autoplaySpeed = this.getParam("autoplaySpeed");
            this.imagesSrc = this.getParam("images_src");
            this.getImages();
            this._makeView();
        },_makeView: function() {
            this.$main.css({position: "absolute",width: "100%",height: "100%",margin: "0",padding: "0",top: "0",left: "0",'text-align': "center",display: "none"});
            this.$imagesHolder.css({position: "absolute"});
            this.$view.css({position: "absolute",width: "100%",height: "100%",overflow: "hidden"});
            this.$view.append(this.$imagesHolder);
            this.$main.append(this.$view);
            this.$root.append(this.$main);
            $("*", this.$main).bind(this.mouseDownBind, $.proxy(this.clearSmall, this));
            $("*", this.$view).unbind(this.mouseDownBind);
            this.loadImage(0);
        },_mouseWheelControl: function(event, delta) {
            this.clearSmall();
            this.speedDeg += delta * this.mouse_wheel_speed;
            return false;
        },getToFront: function() {
            if (this.currentImage >= 0 && this.currentImage < this.numImages) {
                if (this.currentImage != this.oldImage) {
                    if (this.oldImage !== -1) {
                        this.images[this.oldImage].css("display", "none");
                    } else {
                        for (var i = 0; i < this.numImages; i++) {
                            this.images[i].css("display", "none");
                        }
                    }
                    this.images[this.currentImage].css("display", "block");
                    this.oldImage = this.currentImage;
                    this._changeFocus();
                    $(this).trigger("move", this.currentImage);
                }
            }
        },loadImage: function(num) {
            var img = new Image;
            var _this = this;
            img.onload = function() {
                if (num === 0) {
                    $(_this).trigger("loadImageStart");
                    _this.iniWidth = img.width;
                    _this.iniHeight = img.height;
                    _this.zoom = 1;
                    _this.zoomCurrent = _this.zoom;
                    _this.minZoom = _this.zoom;
                    _this.$imagesHolder.css({height: "100%",width: "100%",position: "absolute"});
                }
                var perc = Math.round(num / _this.options.total_frames * 100);
                var params = [num, perc];
                $(_this).trigger("loadImageProgress", params);
                $(img).css({position: "relative",width: "100%",height: "auto"});
                $(img).addClass("3dweb");
                var $image = $("<div rel='" + num + "'></div>");
                $image.css({position: "absolute",width: "100%",height: "100%"});
                $image.append(img);
                _this.$imagesHolder.append($image);
                _this.images.push($image);
                $image.bind(this.mouseDownBind, $.proxy(_this.clearSmall, _this));
                if (num === _this.imagesSrc.length - 1) {
                    _this._loadImageEnd();
                } else {
                    _this.loadImage(++num);
                }
            };
            img.src = this.imagesSrc[num];
        },_loadImageEnd: function() {
            this.currentImage = 0;
            this.numImages = this.imagesSrc.length;
            if (!this.just_load) {
                this.startPresentation();
            }
            $(this).trigger("loadImageEnd");
            this.started = true;
        },startPresentation: function() {
            this.updateImage();
            if (this.autoplay) {
                this.speedDegDefault = this.autoplaySpeed;
            }
            this.dragAndDrop();
            var _this = this;
            this.$el.fadeTo(20, 0, function() {
                _this.$main.fadeTo(20, 1);
            });
        },onRotateMove: function(e) {
            if (this.ismobile) {
                if (this.ignored != e.pageX && this.ignore2 > 2) {
                    this.currentX = e.pageX;
                } else if (this.ignore2 <= 2) {
                    this.ignored = e.pageX;
                    this.ignore2++;
                }
            } else {
                this.currentX = e.pageX;
            }
        },onRotateUp: function() {
            this.dragging = false;
            this._changeFocus();
            $(this).trigger("stopDragging", this.currentImage);
            for (var i = 0; i < this.numImages; i++) {
                if (i != this.currentImage) {
                    this.images[i].css("display", "none");
                }
            }
        },startDrag: function(e) {
            this.ignore2 = 0;
            this.goToDegree = false;
            $(this).trigger("startDragging", this.currentImage);
            this._removeFocus();
            this.positionClickedX = e.pageX;
            this.oldDif = 0;
            this.degWhenClicked = this.degrees;
            this.currentX = this.positionClickedX;
            this.dragging = true;
            $(document).bind(this.mouseMoveBind, $.proxy(this.onRotateMove, this));
            $(document).bind(this.mouseUpBind, $.proxy(this.onRotateUp, this));
            //this.annoyingThing();
            return false;
        },dragAndDrop: function() {
            var _this = this;
            this.preventDragDefault(this.$imagesHolder);
            this.$imagesHolder.hover(function() {
                $(this).trigger("hover", this.currentImage);
            }, function() {
            });
            this.$imagesHolder.bind(this.mouseDownBind, $.proxy(this.startDrag, this));
        },_removeFocus: function() {
            clearTimeout(this.focusTimer);
            this.focusing = false;
            this.focused = false;
            this.focusedNum = -1;
        },_changeFocus: function() {
            if (this.focusable) {
                this._removeFocus();
            }
        },_updateFocus: function() {
            if (this.focusable) {
                clearTimeout(this.focusTimer);
                this.focusing = true;
            }
        },rotateToDegree: function(degree) {
            this.goToDegree = true;
            this.goToDegreeNum = degree;
        },rotateToNumber: function(number, callback) {
            var degree = Math.round(360 / this.numImages * number);
            this.rotateToDegree(degree, callback);
        },getCurrentNumber: function() {
            return this.currentImage;
        },calculateDegree: function() {
            if (!this.dragging && !this.goToDegree) {
                var degreeEase = (this.speedDegDefault - this.speedDeg) / this.ease;
                this.speedDeg += degreeEase;
                this.degrees += Math.round(this.speedDeg);
            } else if (this.goToDegree) {
                var degreeDifference = this.goToDegreeNum - this.degrees;
                if (degreeDifference > 180) {
                    degreeDifference -= 360;
                }
                if (degreeDifference < -180) {
                    degreeDifference += 360;
                }
                degreeEase = degreeDifference / this.ease;
                this.degrees += degreeEase;
                if (Math.round(this.degrees) == this.goToDegreeNum) {
                    this.goToDegree = false;
                    this.degrees = Math.round(this.degrees);
                }
            }
            while (this.degrees > 360) {
                this.degrees -= 360;
            }
            while (this.degrees < -360) {
                this.degrees += 360;
            }
            var degreesCalc = this.degrees;
            if (this.degrees < 0) {
                degreesCalc = 360 + this.degrees;
            }
            this.currentImage = Math.round(degreesCalc / 360 * this.numImages);
            if(this.currentImage == this.numImages){
                this.currentImage = 0;
            }
            if (this.reverse) {
                if (this.currentImage != 0) {
                    this.currentImage = this.numImages - this.currentImage;
                }
            }
            this.getToFront();
        },updateImage: function() {
            this.calculateDegree();
            if (this.dragging) {
                var dif = this.positionClickedX - this.currentX;
                if (this.ismobile) {
                    if (dif < 20 && dif > -20) {
                        dif = 0;
                    }
                    dif /= 2;
                }
                var change = dif - this.oldDif;
                this.speedDeg = Math.round(change * this.inertia);
                this.oldDif = dif;
                this.degrees = this.degWhenClicked + dif;
            }
            setTimeout($.proxy(this.updateImage, this), this.refreshRate);
        },defaultsCheck: function() {
            if (typeof this.options.target_id !== "undefined") {
                if ($("#" + this.options.target_id)) {
                    this.targetEl = $("#" + this.options.target_id);
                } else {
                    this.exception("no target found with id " + this.options.target_id + " for 3D Presentation");
                }
            } else {
                this.targetEl = this.$el;
            }
            this.ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
            this.isIE = false;
            this.mouseDownBind = "mousedown";
            this.mouseMoveBind = "mousemove";
            this.mouseUpBind = "mouseup";
            this.refreshRate = 10;
            if (this.ismobile) {
                this.mouseDownBind = "vmousedown";
                this.mouseMoveBind = "vmousemove";
                this.mouseUpBind = "vmouseup";
                this.refreshRate = 15;
            }
            this.ignore2 = 0, this.ignored, this.ignored1;
        },getImagesPath: function() {
            var src = this.$el[0].src;
            var filename = src.replace(/^.*[\\\/]/, "");
            var path = src.substring(0, src.length - filename.length);
            this.imagesPath = path;
        },getImages: function() {
            if (this.imagesSrc.length > 0) {
                this.autoFind = false;
                return;
            }
            this.getImagesPath();
            var arrImages = [];
            var src = this.$el.attr("src");
            var extension = src.split(".").pop();
            var index = src.lastIndexOf("/") + 1;
            var cleanFilename = src.substr(index);
            for (var frameNo = 0; ++frameNo <= this.options.total_frames; ) {
                frame = (frameNo < 10 ? "0" : "") + frameNo;
                var newFileName = cleanFilename.replace("." + extension, "_" + frame + "." + extension);
                var filename = this.imagesPath + newFileName;
                arrImages.push(filename);
            }
            if (arrImages.length === 0) {
                this.exception("No images found");
            }
            this.imagesSrc = arrImages;
        },exception: function(msg) {
            if (this.getParam("debug", false)) {
                console.log(msg);
                exit();
            }
        },getParam: function(field, defaultValue) {
            if (typeof defaultValue !== "undefined") {
                return typeof this.options[field] !== "undefined" ? this.options[field] : defaultValue;
            } else {
                return typeof this.options[field] !== "undefined" ? this.options[field] : this[field];
            }
        },preventDragDefault: function($obj) {
            if (this.isIE) {
                $obj.get(0).onselectstart = function() {
                    return false;
                };
            }
            $obj.get(0).onmousedown = function(e) {
                e.preventDefault();
            };
        }};
    jQuery.fn.jq360Viewer = function(element, options) {
        return this.each(function() {
            var instance = new jq360Viewer(this, options);
            instance.start();
            return this;
        });
        new jq360Viewer(this, options);
        return this;
    };
});
